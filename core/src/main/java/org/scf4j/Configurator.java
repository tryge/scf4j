/*
 * Copyright (c) 2012, Michael Thomas Zehender
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name scf4j nor the names of its contributors may be used
 *       to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Michael Thomas Zehender BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.scf4j;

/**
 * @author michael.zehender@me.com
 */
public interface Configurator {
	/**
	 * This method just checks whether the file exists, it doesn't check whether
	 * the data in the file is consistent.
	 *
	 * @return true if the specified configuration file is available, false otherwise
	 * @throws IllegalArgumentException if the interface is not annotated with
	 *         Configuration or one of the methods isn't annotated with
	 *         Property.
	 */
	boolean isAvailable(Class<?> clazz);

	/**
	 * Returns the configuration fo the specified interface, which has to be an
	 * interface annotated with {@code Configuration}. Moreover the methods
	 * have to be annotated with {@code Property}.
	 *
	 * @param clazz to retrieve the configuration for
	 * @param <T> the actual type of the clazz
	 * @return the configuration or null if it doesn't exist
	 * @throws ConfigurationException if there is an error loading the data or
	 *         the data is missing required properties
	 * @throws IllegalArgumentException if the class is not annotated with
	 *         Configuration or one of the methods isn't annotated with
	 *         Property.
	 */
	<T> T getConfiguration(Class<T> clazz) throws ConfigurationException;
}
