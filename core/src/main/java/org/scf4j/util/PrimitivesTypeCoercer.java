/*
 * Copyright (c) 2012, Michael Thomas Zehender
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name scf4j nor the names of its contributors may be used
 *       to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Michael Thomas Zehender BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.scf4j.util;

import org.scf4j.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * @author michael.zehender@me.com
 */
@SuppressWarnings("unchecked")
public class PrimitivesTypeCoercer implements TypeCoercer {
	private final Logger logger = LoggerFactory.getLogger(PrimitivesTypeCoercer.class);

	private Map<Class<?>, TypeCoercer> subs = new HashMap<Class<?>, TypeCoercer>();
	{
		subs.put(String.class, new StringCoercer());
		subs.put(char.class, new CharCoercer());
		subs.put(Character.class, new CharCoercer());
		subs.put(int.class, new IntCoercer());
		subs.put(Integer.class, new IntCoercer());
		subs.put(long.class, new LongCoercer());
		subs.put(Long.class, new LongCoercer());
		subs.put(short.class, new ShortCoercer());
		subs.put(Short.class, new ShortCoercer());
		subs.put(byte.class, new ByteCoercer());
		subs.put(Byte.class, new ByteCoercer());
		subs.put(float.class, new FloatCoercer());
		subs.put(Float.class, new FloatCoercer());
		subs.put(double.class, new DoubleCoercer());
		subs.put(Double.class, new DoubleCoercer());
		subs.put(boolean.class, new BooleanCoercer());
		subs.put(Boolean.class, new BooleanCoercer());
	}

	@Override
	public <T> T coerce(String value, Class<T> to) throws ConfigurationException {
		logger.debug(Messages.getString("try.to.coerce.pstr.to.pcls"), value, to.getCanonicalName());
		TypeCoercer coercer = subs.get(to);
		if (coercer == null) {
			logger.error(Messages.getString("did.not.find.a.handler.for.class.pcls"), to.getCanonicalName());
			throw new ConfigurationException(Messages.getString("type.0.not.supported", to.getCanonicalName()));
		}
		logger.debug(Messages.getString("found.handler.pobj.for.class.pcls"), coercer, to.getCanonicalName());
		return coercer.coerce(value, to);
	}

	private class StringCoercer implements TypeCoercer {
		@Override
		public <T> T coerce(String value, Class<T> to) throws ConfigurationException {
			return to.cast(value);
		}
	}

	private class CharCoercer implements TypeCoercer {
		@Override
		public <T> T coerce(String value, Class<T> to) throws ConfigurationException {
			if (value.length() > 1) {
				value = value.trim();
			}
			if (value.length() > 1) {
				throw new ConfigurationException(Messages.getString("too.many.characters.0", value));
			}
			if (value.length() == 0) {
				throw new ConfigurationException(Messages.getString("empty.value"));
			}
			return (T)Character.valueOf(value.charAt(0));
		}
	}

	private class IntCoercer implements TypeCoercer {
		@Override
		public <T> T coerce(String value, Class<T> to) throws ConfigurationException {
			try {
				return (T)Integer.valueOf(value.trim());
			} catch (NumberFormatException nfe) {
				throw new ConfigurationException(Messages.getString("not.an.integer.value.0", value), nfe);
			}
		}
	}

	private class LongCoercer implements TypeCoercer {
		@Override
		public <T> T coerce(String value, Class<T> to) throws ConfigurationException {
			try {
				return (T)Long.valueOf(value.trim());
			} catch (NumberFormatException nfe) {
				throw new ConfigurationException(Messages.getString("not.a.long.value.0", value), nfe);
			}
		}
	}

	private class ShortCoercer implements TypeCoercer {
		@Override
		public <T> T coerce(String value, Class<T> to) throws ConfigurationException {
			try {
				return (T)Short.valueOf(value.trim());
			} catch (NumberFormatException nfe) {
				throw new ConfigurationException(Messages.getString("not.a.short.value.0", value), nfe);
			}
		}
	}

	private class ByteCoercer implements TypeCoercer {
		@Override
		public <T> T coerce(String value, Class<T> to) throws ConfigurationException {
			try {
				return (T)Byte.valueOf(value);
			} catch (NumberFormatException nfe) {
				throw new ConfigurationException(Messages.getString("not.a.byte.value.0", value), nfe);
			}
		}
	}

	private class FloatCoercer implements TypeCoercer {
		@Override
		public <T> T coerce(String value, Class<T> to) throws ConfigurationException {
			try {
				return (T)Float.valueOf(value);
			} catch (NumberFormatException nfe) {
				throw new ConfigurationException(Messages.getString("not.a.float.value.0", value), nfe);
			}
		}
	}

	private class DoubleCoercer implements TypeCoercer {
		@Override
		public <T> T coerce(String value, Class<T> to) throws ConfigurationException {
			try {
				return (T)Double.valueOf(value);
			} catch (NumberFormatException nfe) {
				throw new ConfigurationException(Messages.getString("not.a.float.value.01", value), nfe);
			}
		}
	}

	private class BooleanCoercer implements TypeCoercer {
		@Override
		public <T> T coerce(String value, Class<T> to) throws ConfigurationException {
			return (T)Boolean.valueOf(value);
		}
	}
}
