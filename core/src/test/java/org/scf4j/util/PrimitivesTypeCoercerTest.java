/*
 * Copyright (c) 2012, Michael Thomas Zehender
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name scf4j nor the names of its contributors may be used
 *       to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Michael Thomas Zehender BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.scf4j.util;

import org.junit.Before;
import org.junit.Test;
import org.scf4j.ConfigurationException;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * @author michael.zehender@me.com
 */
@SuppressWarnings("HardCodedStringLiteral")
public class PrimitivesTypeCoercerTest {
	private PrimitivesTypeCoercer coercer;

	@Before
	public void setUp() {
		coercer = new PrimitivesTypeCoercer();
	}

	@Test(expected = ConfigurationException.class)
	public void testUnsuppoertedCoercion() throws ConfigurationException {
		coercer.coerce("something", Date.class);
	}

	@Test
	public void testCoercionFromStringToString() throws ConfigurationException {
		String value = (String)coercer.coerce("value", returnType("StringReturn"));

		assertEquals("value", value);
	}

	@Test
	public void testCoercionFromStringToChar() throws ConfigurationException {
		char value = (Character)coercer.coerce("a", returnType("charReturn"));

		assertEquals('a', value);
	}

	@Test
	public void testCoercionFromStringToCharacter() throws ConfigurationException {
		Character value = (Character)coercer.coerce("a", returnType("CharacterReturn"));

		assertEquals(Character.valueOf('a'), value);
	}

	@Test(expected = ConfigurationException.class)
	public void testInvalidCharCoercion_1() throws ConfigurationException {
		coercer.coerce("", returnType("CharacterReturn"));
	}

	@Test(expected = ConfigurationException.class)
	public void testInvalidCharCoercion_2() throws ConfigurationException {
		coercer.coerce("aa", returnType("CharacterReturn"));
	}

	@Test(expected = ConfigurationException.class)
	public void testInvalidCharCoercion_3() throws ConfigurationException {
		coercer.coerce(" \t\r\n", returnType("CharacterReturn"));
	}

	@Test
	public void testCoercionFromStringToInt() throws ConfigurationException {
		int value = (Integer)coercer.coerce("1", returnType("intReturn"));

		assertEquals(1, value);
	}

	@Test
	public void testCoercionFromStringToInteger() throws ConfigurationException {
		Integer value = (Integer)coercer.coerce("1", returnType("IntegerReturn"));

		assertEquals(Integer.valueOf(1), value);
	}

	@Test
	public void testCoercionFromStringToInteger_2() throws ConfigurationException {
		Integer value = (Integer)coercer.coerce("1 ", returnType("IntegerReturn"));

		assertEquals(Integer.valueOf(1), value);
	}

	@Test(expected = ConfigurationException.class)
	public void testInvalidIntCoercion() throws ConfigurationException {
		coercer.coerce("a", returnType("intReturn"));
	}

 	@Test
	public void testCoercionFromStringToLong_1() throws ConfigurationException {
		long value = (Long)coercer.coerce("1", returnType("longReturn"));

		assertEquals(1L, value);
	}

	@Test
	public void testCoercionFromStringToLong_2() throws ConfigurationException {
		Long value = (Long)coercer.coerce("1", returnType("LongReturn"));

		assertEquals(Long.valueOf(1L), value);
	}

	@Test(expected = ConfigurationException.class)
	public void testInvalidLongCoercion() throws ConfigurationException {
		coercer.coerce("a", returnType("longReturn"));
	}

	@Test
	public void testCoercionFromStringToShort_1() throws ConfigurationException {
		short value = (Short)coercer.coerce("1", returnType("shortReturn"));

		assertEquals((short)1, value);
	}

	@Test
	public void testCoercionFromStringToShort_2() throws ConfigurationException {
		Short value = (Short)coercer.coerce("1", returnType("ShortReturn"));

		assertEquals(Short.valueOf((short)1), value);
	}

	@Test(expected = ConfigurationException.class)
	public void testInvalidShortCoercion() throws ConfigurationException {
		coercer.coerce("a", returnType("shortReturn"));
	}

	@Test
	public void testCoercionFromStringToByte_1() throws ConfigurationException {
		byte value = (Byte)coercer.coerce("1", returnType("byteReturn"));

		assertEquals((byte)1, value);
	}

	@Test
	public void testCoercionFromStringToByte_2() throws ConfigurationException {
		Byte value = (Byte)coercer.coerce("1", returnType("ByteReturn"));

		assertEquals(Byte.valueOf((byte) 1), value);
	}

	@Test(expected = ConfigurationException.class)
	public void testInvalidByteCoercion() throws ConfigurationException {
		coercer.coerce("a", returnType("byteReturn"));
	}

	@Test
	public void testCoercingFromStringToFloat_1() throws ConfigurationException {
		float value = (Float)coercer.coerce("1.1", returnType("floatReturn"));

		assertEquals(1.1f, value, 0.000001);
	}

	@Test
	public void testCoercingFromStringToFloat_2() throws ConfigurationException {
		Float value = (Float)coercer.coerce("1.1", returnType("FloatReturn"));

		assertEquals(Float.valueOf(1.1f), value);
	}

	@Test(expected = ConfigurationException.class)
	public void testInvalidFloatCoercion() throws ConfigurationException {
		coercer.coerce("a", returnType("floatReturn"));
	}

	@Test
	public void testCoercionFromStringToDouble_1() throws ConfigurationException {
		double value = (Double)coercer.coerce("1.1", returnType("doubleReturn"));

		assertEquals(1.1, value, 0.000001);
	}

	@Test
	public void testCoercionFromStringToDouble_2() throws ConfigurationException {
		Double value = (Double)coercer.coerce("1.1", returnType("DoubleReturn"));

		assertEquals(Double.valueOf(1.1), value);
	}

	@Test(expected = ConfigurationException.class)
	public void testInvalidDoubleCoercion() throws ConfigurationException {
		coercer.coerce("a", returnType("doubleReturn"));
	}

	@Test
	public void testCoercionFromStringToBoolean_1() throws ConfigurationException {
		boolean value = (Boolean)coercer.coerce("true", returnType("booleanReturn"));

		assertEquals(true, value);
	}

	@Test
	public void testCoercionFromStringToBoolean_2() throws ConfigurationException {
		Boolean value = (Boolean)coercer.coerce("false", returnType("BooleanReturn"));

		assertEquals(false, value);
	}

	private Class<?> returnType(String method) {
		try {
			return CoerceContainer.class.getMethod(method).getReturnType();
		} catch (NoSuchMethodException e) {
			assertFalse(true);
			return null;
		}
	}

	@SuppressWarnings("UnusedDeclaration")
	public interface CoerceContainer {
		String StringReturn();
		char charReturn();
		Character CharacterReturn();
		int intReturn();
		Integer IntegerReturn();
		long longReturn();
		Long LongReturn();
		short shortReturn();
		Short ShortReturn();
		byte byteReturn();
		Byte ByteReturn();
		float floatReturn();
		Float FloatReturn();
		double doubleReturn();
		Double DoubleReturn();
		boolean booleanReturn();
		Boolean BooleanReturn();
	}
}
