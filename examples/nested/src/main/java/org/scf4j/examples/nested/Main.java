/*
 * Copyright (c) 2012, Michael Thomas Zehender
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name scf4j nor the names of its contributors may be used
 *       to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Michael Thomas Zehender BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.scf4j.examples.nested;

import org.scf4j.Configuration;
import org.scf4j.ConfigurationException;
import org.scf4j.Property;
import org.scf4j.props.PropertyConfigurator;

import java.io.File;

/**
 * This example demonstrates how easy it is to define a nested property,
 * in short you just use another interface annotated with {@code Configuration}
 * as the return type within your main (or nested) configuration interface.
 *
 * @author michael.zehender@me.com
 */
public class Main {
	private static void usage() {
		System.out.println("Usage:");
		System.out.println("\tjava org.scf4j.examples.nested.Main <configRoot>");
		System.out.println();
		usageConfigRoot();
	}

	private static void usageConfigRoot() {
		System.out.println("The parameter configRoot has to be an existing directory, and");
		System.out.println("and this directory must contain the following file:");
		System.out.println("\tnested.properties");
		System.out.println();
		System.out.println("Moreover the file must contain following properties:");
		System.out.println("\tnestedProp.string");
		System.out.println("\tnestedProp.int");
		System.out.println();
		System.out.println("Of course the values have to be valid according to their types.");
		System.exit(1);
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			usage();
		}

		File configRoot = new File(args[0]);
		if (!configRoot.exists() || !configRoot.isDirectory()) {
			usage();
		}

		PropertyConfigurator configurator = new PropertyConfigurator();
		configurator.setConfigurationRoot(configRoot);

		try {
			Config config = configurator.getConfiguration(Config.class);

			System.out.println("Your configuration is: ");
			System.out.println("\tnestedProp.string: " + config.getNested().getString());
			System.out.println("\tnestedProp.int:    " + config.getNested().getInt());
		} catch (ConfigurationException e) {
			System.out.println(e.getMessage());
			System.out.println();
			usage();
		}
	}

	@Configuration("nested")
	public static interface Config {
		@Property(id = "nestedProp")
		Nested getNested();
	}

	@Configuration
	public static interface Nested {
		@Property(id = "string")
		String getString();

		@Property(id = "int")
		int getInt();
	}
}
