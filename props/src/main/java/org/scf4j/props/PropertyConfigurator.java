/*
 * Copyright (c) 2012, Michael Thomas Zehender
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name scf4j nor the names of its contributors may be used
 *       to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Michael Thomas Zehender BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.scf4j.props;

import org.scf4j.Configuration;
import org.scf4j.ConfigurationException;
import org.scf4j.Configurator;
import org.scf4j.Property;
import org.scf4j.util.PrimitivesTypeCoercer;
import org.scf4j.util.TypeCoercer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author michael.zehender@me.com
 */
public class PropertyConfigurator implements Configurator {
	private static final String EXTENSION = ".properties";

	private final Logger logger = LoggerFactory.getLogger(PropertyConfigurator.class);

	@Override
	public boolean isAvailable(Class<?> clazz) {
		File file = getFile(defendClazz(clazz));

		logger.debug(Messages.getString("configuration.file.for.clazz.pcls.is.pfile"), clazz.getCanonicalName(), file.getAbsolutePath());

		return file.exists();
	}

	private Configuration defendClazz(Class<?> clazz) {
		if (!clazz.isInterface()) {
			throw new IllegalArgumentException(Messages.getString("configuration.must.be.an.interface"));
		}

		Configuration configuration = clazz.getAnnotation(Configuration.class);
		if (configuration == null) {
			throw new IllegalArgumentException(Messages.getString("not.a.configuration.0", clazz.getCanonicalName()));
		}
		return configuration;
	}

	private File getFile(Configuration configuration) {
		return new File(
			configurationRoot,
			configuration.value() + EXTENSION
		);
	}



	@Override
	public <T> T getConfiguration(Class<T> clazz) throws ConfigurationException {
		File file = getFile(defendClazz(clazz));

		logger.debug(Messages.getString("configuration.file.for.clazz.pcls.is.pfile"), clazz.getCanonicalName(), file.getAbsolutePath());

		Properties properties = loadProperties(file);

		return getConfiguration(clazz, properties, "");
	}

	private Properties loadProperties(File file) throws ConfigurationException {
		FileInputStream in = null;
		try {

			in = new FileInputStream(file);
			Properties properties = new Properties();
			properties.load(in);
			return properties;
		} catch (Exception e) {
			logger.error(Messages.getString("could.not.load.the.configuration"), e);
			throw new ConfigurationException(Messages.getString("could.not.load.the.configuration"), e);
		} finally {
			if (in != null) {
				try { in.close(); } catch (IOException e) { /* ignore */}
			}
		}
	}

	private <T> T getConfiguration(Class<T> clazz, Properties properties, String path) throws ConfigurationException {
		final Map<Method, Object> propertyMapping = mapProperties(clazz, properties, path);

		return clazz.cast(Proxy.newProxyInstance(clazz.getClassLoader(), new Class[]{clazz}, new InvocationHandler() {
			@Override
			public Object invoke(Object obj, Method method, Object[] objects) throws Throwable {
				return propertyMapping.get(method);
			}
		}));
	}

	private Map<Method, Object> mapProperties(Class<?> clazz, Properties properties, String path) throws ConfigurationException {
		Map<Method, Object> propertyMapping = new HashMap<Method, Object>();

		for (Method method : clazz.getDeclaredMethods()) {
			mapProperty(propertyMapping, properties, path, method);
		}
		return propertyMapping;
	}

	private void mapProperty(Map<Method, Object> propertyMapping, Properties properties, String path, Method method) throws ConfigurationException {
		Property property = defendMethod(method);

		String separator = path.equals("") ? "" : ".";
		String propertyPath = path + separator + property.id();
		String value = properties.getProperty(propertyPath);

		logger.debug(Messages.getString("try.to.map.and.coerce.property.pstr"), propertyPath);

		Object nested = checkNested(properties, method.getReturnType(), propertyPath);
		if (nested != null) {
			propertyMapping.put(method, nested);
		} else  if (value != null) {
			propertyMapping.put(method, getCoercer().coerce(value, method.getReturnType()));
		} else if (property.optional()) {
			propertyMapping.put(method, getCoercer().coerce(property.value(), method.getReturnType()));
		} else {
			throw new ConfigurationException(Messages.getString("required.property.0.is.missing", propertyPath));
		}
	}

	private Property defendMethod(Method method) {
		if (method.getReturnType().equals(void.class)) {
			throw new IllegalArgumentException(Messages.getString("found.void.return.type.in.property.method.0", method.getName()));
		}

		Property property = method.getAnnotation(Property.class);
		if (property == null) {
			throw new IllegalArgumentException(Messages.getString("method.0.is.not.annotated.with.property", method.getName()));
		}
		return property;
	}

	private Object checkNested(Properties properties, Class<?> returnType, String propertyPath) throws ConfigurationException {
		if (returnType.isInterface()) {
			Configuration configuration = returnType.getAnnotation(Configuration.class);

			if (configuration != null) {
				logger.debug(Messages.getString("found.nested.configuration.pcls.path.pstr"), returnType.getCanonicalName(), propertyPath);

				return getConfiguration(returnType, properties, propertyPath);
			}
		}
		return null;
	}

	private TypeCoercer getCoercer() {
		if (coercer == null) {
			coercer = new PrimitivesTypeCoercer();
		}
		return coercer;
	}


	@PostConstruct
	public void checkConstruction() {
		if (configurationRoot == null) {
			throw new IllegalStateException(Messages.getString("propertyconfigurator.is.missing.the.property.configurationroot"));
		}
		if (!configurationRoot.isDirectory()) {
			throw new IllegalStateException(Messages.getString("propertyconfigurator.s.root.directory.is.not.a.directory.0", configurationRoot.getAbsolutePath()));
		}
	}



	// INJECTED
	private TypeCoercer coercer;
	private File configurationRoot;

	public void setTypeCoercer(TypeCoercer coercer) {
		this.coercer = coercer;
	}

	public void setConfigurationRoot(File configurationRoot) {
		this.configurationRoot = configurationRoot;
	}
}

