/*
 * Copyright (c) 2012, Michael Thomas Zehender
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name scf4j nor the names of its contributors may be used
 *       to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Michael Thomas Zehender BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.scf4j.props;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.scf4j.Configuration;
import org.scf4j.ConfigurationException;
import org.scf4j.Property;
import org.scf4j.util.TypeCoercer;

import java.io.*;
import java.util.Properties;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author michael.zehender@me.com
 */
public class PropertyConfiguratorTest {
	private static final String TEST_PROPS = "autotest.properties";
	private static final String NOTAN_PROPERTIES = "notan.properties";
	private static final String VOID_PROPERTIES = "void.properties";
	private static final String NESTED_PROPERTIES = "composite.properties";
	private static final String TEST_VALUE = "test";
	public static final String TEST_NESTED_NESTED1 = "testNested.nested1";
	public static final String TEST_NESTED_NESTED2_D1 = "testNested.nested2.d1";
	public static final String TEST_NESTED2_NESTED1 = "testNested2.nested1";
	public static final String TEST_NESTED2_NESTED2_D1 = "testNested2.nested2.d1";
	public static final String ST4ND4RD = "st4nd4rd";
	public static final String N1_1 = "n1.1";
	public static final String N1_2 = "n1.2";
	public static final String N2_1 = "n2.1";
	public static final String N2_2 = "n2.2";

	private PropertyConfigurator configurator;

	@Rule
	public TemporaryFolder folder = new TemporaryFolder();

	@Before
	public void setUp() {
		configurator = new PropertyConfigurator();
		configurator.setConfigurationRoot(folder.getRoot());

	}

	@Test(expected = IllegalStateException.class)
	public void checkConstructionShouldThrowDueToNull() {
		configurator = new PropertyConfigurator();
		configurator.checkConstruction();
	}

	@Test(expected = IllegalStateException.class)
	public void checkConstructionShouldThrowDueToFile() throws IOException {
		configurator = new PropertyConfigurator();
		configurator.setConfigurationRoot(folder.newFile(TEST_PROPS));
		configurator.checkConstruction();
	}

	@Test
	public void checkConstructionOk() {
		configurator.checkConstruction();
	}

	@Test(expected = IllegalArgumentException.class)
	public void checkIllegalTestConfig() {
		configurator.isAvailable(IllegalTestConfig.class);
	}

	@Test(expected = IllegalArgumentException.class)
	public void checkIllegalTestClass() {
		configurator.isAvailable(IllegalTestClass.class);
	}

	@Test
	public void checkCoreConfigNotAvailable() {
		assertFalse(configurator.isAvailable(TestString.class));
	}

	@Test
	public void checkCoreConfigAvailable() throws IOException {
		makeMinimalConfig(folder.newFile(TEST_PROPS));

		assertTrue(configurator.isAvailable(TestString.class));
	}

	@Test(expected = ConfigurationException.class)
	public void checkNoConfiguration() throws IOException, ConfigurationException {
		configurator.getConfiguration(TestString.class);
	}

	@Test(expected = ConfigurationException.class)
	public void checkMalformedConfiguration() throws IOException, ConfigurationException {
		makeMalformedConfig(folder.newFile(TEST_PROPS));

		configurator.getConfiguration(TestString.class);
	}

	@Test(expected = IllegalArgumentException.class)
	public void checkConfigurationWithVoidMethodThrows() throws IOException, ConfigurationException {
		makeMinimalConfig(folder.newFile(VOID_PROPERTIES));

		configurator.getConfiguration(IllegalTestMethod.class);
	}

	@Test(expected = IllegalArgumentException.class)
	public void checkConfigurationWithIllegalMethodThrows() throws IOException, ConfigurationException {
		makeMinimalConfig(folder.newFile(NOTAN_PROPERTIES));

		configurator.getConfiguration(IllegalTestMethod2.class);
	}

	@Test
	public void checkStringProperty() throws IOException, ConfigurationException {
		makeMinimalConfig(folder.newFile(TEST_PROPS));
		TestString config = configurator.getConfiguration(TestString.class);

		assertNotNull(config);
		assertEquals(TEST_VALUE, config.getStringProperty());
	}

	@Test(expected = ConfigurationException.class)
	public void checkStringPropertyAbsent() throws IOException, ConfigurationException {
		makeAlternateConfig(folder.newFile(TEST_PROPS));
		configurator.getConfiguration(TestString.class);
	}

	@Test
	public void checkMessageWhenStringPropertyAbsent() throws IOException {
		makeAlternateConfig(folder.newFile(TEST_PROPS));

		try {
			configurator.getConfiguration(TestString.class);
			fail();
		} catch (ConfigurationException e) {
			assertEquals("Required property: " + TestOtherProp.STRING_PROPERTY + " is missing.", e.getMessage()); //NON-NLS
		}
	}

	@Test
	public void testOptionalStringProperty_1() throws IOException, ConfigurationException {
		makeMinimalConfig(folder.newFile(TEST_PROPS));

		TestOptString config = configurator.getConfiguration(TestOptString.class);
		assertEquals("default", config.getStringProperty());
	}

	@Test
	public void testOptionalStringProperty_2() throws IOException, ConfigurationException {
		makeAlternateConfig(folder.newFile(TEST_PROPS));

		TestOptString config = configurator.getConfiguration(TestOptString.class);
		assertEquals(TEST_VALUE, config.getStringProperty());
	}

	@Test
	public void testOtherProperties() throws IOException, ConfigurationException {
		TypeCoercer coercer = mock(TypeCoercer.class);
		Object obj = mock(Object.class);

		when(coercer.coerce(TEST_VALUE, Object.class)).thenReturn(obj);

		makeMinimalConfig(folder.newFile(TEST_PROPS));

		configurator.setTypeCoercer(coercer);
		TestOtherProp config = configurator.getConfiguration(TestOtherProp.class);
		assertSame(obj, config.getObjectProperty());
	}

	@Test
	public void testNestedProperties() throws IOException, ConfigurationException {
		makeNestedConfig(folder.newFile(NESTED_PROPERTIES));

		TestCompositeProp config = configurator.getConfiguration(TestCompositeProp.class);

		assertEquals(ST4ND4RD, config.getStandard());

		TestNestedProp nested1 = config.getNested();
		TestNestedProp nested2 = config.getNested2();

		assertNotNull(nested1);
		assertNotNull(nested2);

		assertEquals(N1_1, nested1.getNested1());
		assertEquals(N1_2, nested1.getNested2().getDoubleNested());
		assertEquals(N2_1, nested2.getNested1());
		assertEquals(N2_2, nested2.getNested2().getDoubleNested());
	}

	@Test
	public void checkMessageWhenNestedPropertyIsMissing() throws IOException {
		makeMissingNestedConfig(folder.newFile(NESTED_PROPERTIES));

		try {
			configurator.getConfiguration(TestCompositeProp.class);
			fail();
		} catch (ConfigurationException e) {
			assertEquals("Required property: testNested.nested1 is missing.", e.getMessage());
		}
	}



	private void makeMinimalConfig(File file) throws IOException {
		FileOutputStream out = new FileOutputStream(file);
		try {
			Properties properties = new Properties();
			properties.setProperty(TestOtherProp.STRING_PROPERTY, TEST_VALUE);
			properties.store(out, null);
		} finally {
			closeSilently(out);
		}
	}

	private void makeAlternateConfig(File file) throws IOException {
		FileOutputStream out = new FileOutputStream(file);
		try {
			Properties properties = new Properties();
			properties.setProperty(TestOptString.OPT_STRING_PROPERTY, TEST_VALUE);
			properties.store(out, null);
		} finally {
			closeSilently(out);
		}
	}

	private void makeMalformedConfig(File file) throws IOException {
		FileOutputStream out = new FileOutputStream(file);
		PrintStream print = new PrintStream(out);
		try {
			print.println("stringProperty=invalid text: \\u"); // NON-NLS
		} finally {
			print.close();
		}
	}

	private void makeNestedConfig(File file) throws IOException {
		FileOutputStream out = new FileOutputStream(file);
		try {
			Properties properties = new Properties();
			properties.setProperty(TestCompositeProp.STANDARD, ST4ND4RD);
			properties.setProperty(TEST_NESTED_NESTED1, N1_1);
			properties.setProperty(TEST_NESTED_NESTED2_D1, N1_2);
			properties.setProperty(TEST_NESTED2_NESTED1, N2_1);
			properties.setProperty(TEST_NESTED2_NESTED2_D1, N2_2);
			properties.store(out, null);
		} finally {
			closeSilently(out);
		}
	}

	private void makeMissingNestedConfig(File file) throws IOException {
		FileOutputStream out = new FileOutputStream(file);
		try {
			Properties properties = new Properties();
			properties.setProperty(TestCompositeProp.STANDARD, ST4ND4RD);
			properties.setProperty(TEST_NESTED_NESTED2_D1, N1_2);
			properties.setProperty(TEST_NESTED2_NESTED1, N2_1);
			properties.setProperty(TEST_NESTED2_NESTED2_D1, N2_2);
			properties.store(out, null);
		} finally {
			closeSilently(out);
		}
	}

	private void closeSilently(Closeable closeable) {
		try {
			closeable.close();
		} catch (IOException e) {
			// ignore
		}
	}

	public interface IllegalTestConfig {
	}

	@Configuration("class")
	public static class IllegalTestClass {
	}

	@Configuration("void")
	public interface IllegalTestMethod {
		@SuppressWarnings("UnusedDeclaration")
		@Property(id = "voidProperty")
		void getVoidProperty();
	}

	@Configuration("notan")
	public interface IllegalTestMethod2 {
		@SuppressWarnings("UnusedDeclaration")
		String getStringProperty();
	}

	@Configuration("autotest")
	public interface TestString {
		@Property(id = TestOtherProp.STRING_PROPERTY)
		String getStringProperty();
	}

	@Configuration("autotest")
	public interface TestOptString {
		String OPT_STRING_PROPERTY = "optStringProperty";

		@Property(id = OPT_STRING_PROPERTY, optional = true, value = "default")
		String getStringProperty();
	}

	@Configuration("autotest")
	public interface TestOtherProp {
		String STRING_PROPERTY = "stringProperty";

		@Property(id = STRING_PROPERTY)
		Object getObjectProperty();
	}

	@Configuration
	public interface TestDoubleNestedProp {
		@Property(id = "d1")
		String getDoubleNested();
	}

	@Configuration
	public interface TestNestedProp {
		@Property(id = "nested1")
		String getNested1();

		@Property(id = "nested2")
		TestDoubleNestedProp getNested2();
	}

	@Configuration("composite")
	public interface TestCompositeProp {
		String STANDARD = "standard";

		@Property(id = "testNested")
		TestNestedProp getNested();

		@Property(id = "testNested2")
		TestNestedProp getNested2();

		@Property(id = STANDARD)
		String getStandard();
	}
}
